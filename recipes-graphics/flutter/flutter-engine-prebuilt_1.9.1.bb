SUMMARY = "Prebuilt flutter engine(1.9.1)"
HOMEPAGE = "https://github.com/ardera/flutter-pi/commits/engine-binaries"
LICENSE = "CLOSED"
SECTION = "flutter"

S="${WORKDIR}/git"

SRC_URI = "git://github.com/ardera/flutter-pi.git;protocol=https;branch=engine-binaries"
SRCREV = "6dd9855c8ff7273b97fa6d1fa3375707fdee7db6"

do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
    install -d ${D}${libdir}
    install -m 744 ${S}/libflutter_engine.so ${D}${libdir}/libflutter_engine.so

    install -m 744 ${S}/icudtl.dat ${D}${libdir}/icudtl.dat

    install -d ${D}${includedir}
    cp ${S}/flutter_embedder.h ${D}${includedir}
}

FILES_${PN} += "${libdir}/icudtl.dat \
                ${libdir}/libflutter_engine.so"

#ALLOW_EMPTY_${PN} = "1"
INSANE_SKIP_${PN}-dev += "dev-elf"
