SUMMARY = "A light-weight Flutter Engine Embedder for Raspberry Pi that runs without X."
HOMEPAGE = "https://github.com/ardera/flutter-pi"
SECTION = "flutter"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=49fada46694956cdf2fc0292d72d888c"

DEPENDS = "flutter-engine-prebuilt linux-libc-headers libdrm virtual/libgl virtual/libgles2"
RDEPENDS_${PN} += " flutter-engine-prebuilt "

SRC_URI = "git://github.com/ardera/flutter-pi.git;protocol=https;branch=master"
SRCREV = "e8f86132c1985708335fcba52463ac88124f0b60"

SRC_URI += " \
            file://0001-makefile-make-compatible-for-cross-compiling.patch \
            "

S = "${WORKDIR}/git"

inherit pkgconfig

do_install() {
    install -d ${D}/${bindir}
    cp -a ${S}/out/flutter-pi ${D}/${bindir}
}

FILES_${PN} = "${bindir}/flutter-pi"
INSANE_SKIP_${PN} += "dev-deps"
